import Vue from 'vue'

const TOOLTIP_SPACE = 3
const TOOLTIP_ARROW_SIZE = 5

Vue.directive('tooltip', {
  inserted(
    el,
    { name, value: { className = '', message = '', position = 'bottom' } }
  ) {
    if (name === 'tooltip' && el.scrollHeight > el.offsetHeight) {
      el.className = `${el.className} global-tooltip-element`

      const tooltip = document.createElement('div')
      tooltip.innerHTML = message
      tooltip.className = `global-tooltip ${position}${
        className ? ` ${className}` : ''
      }`
      el.append(tooltip)

      const {
        width: elementWidth,
        height: elementHeight,
        left: elementLeft,
        top: elementTop,
        right: elementRight,
        bottom: elementBottom
      } = el.getBoundingClientRect()

      // horizontal
      const tooltipWidth = tooltip.offsetWidth
      let left = 0
      switch (position) {
        case 'left':
          left =
            elementLeft - tooltipWidth - TOOLTIP_ARROW_SIZE * 2 - TOOLTIP_SPACE
          break
        case 'right':
          left = elementRight + TOOLTIP_ARROW_SIZE * 2 + TOOLTIP_SPACE
          break
        case 'top':
        case 'bottom':
        default:
          left = (elementWidth - tooltipWidth) / 2 + elementLeft
          break
      }

      // vertical
      const tooltipHeight = tooltip.offsetHeight
      let top = 0
      switch (position) {
        case 'left':
        case 'right':
          top = (elementHeight - tooltipHeight) / 2 + elementTop
          break
        case 'top':
          top =
            elementTop - tooltipHeight - TOOLTIP_ARROW_SIZE * 2 - TOOLTIP_SPACE
          break
        case 'bottom':
        default:
          top = elementBottom + TOOLTIP_ARROW_SIZE * 2 + TOOLTIP_SPACE
          break
      }

      tooltip.style.cssText = `left: ${left}px; top: ${top}px;`
    }
  }
})
