import Vue from 'vue'
import VueI18n from 'vue-i18n'
import { ZH_HANT } from '~/constants/i18n'
import zhHant from '~/locales/zh-hant.js'

Vue.use(VueI18n)

export default ({ app, store }) => {
  app.i18n = new VueI18n({
    locale: store.state.locale,
    fallbackLocale: ZH_HANT,
    messages: {
      [ZH_HANT]: zhHant
    }
  })
}
