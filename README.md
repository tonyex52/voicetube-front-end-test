# voicetube-Front-end-test

> VoiceTube Front-end Test

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# Unit test
$ yarn test // watch and only modified
$ yarn test:watch // watch all and display coverage

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
