module.exports = {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: 'VoiceTube 前端測試',
    meta: [
      { charset: 'UTF-8' },
      {
        name: 'keywords',
        content: 'VoiceTube, Front-end, interview, filter, media, Vue, Nuxt'
      },
      {
        name: 'keywords',
        content: '紅點子, 前端, 面試, 過濾, 媒體, Vue, Nuxt'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, user-scalable=no'
      },
      { name: 'author', content: 'Tony Wang 王仲揚' },
      {
        name: 'generator',
        content: 'Visual Studio Code/1.41.1(Win10|MacOS Catalina)'
      },
      { name: 'Creation-Date', content: '05-feb-2020 17:00:00' },
      {
        hid: 'description',
        name: 'description',
        content: 'VoiceTube Front-end Interview Coding'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/vt-logo-white.png' },
      { rel: 'author', href: 'tonyex52@gmail.com' }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['~/styles/global.less'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/i18n.js', '~/plugins/tooltip.js'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
