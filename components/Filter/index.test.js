import { shallowMount } from '@vue/test-utils'
import List from './List.vue'
import Filter from './index.vue'

describe('Filter', () => {
  const onClick = jest.fn()
  const defaultProps = {
    filterList: [
      {
        listName: 'test list name 1',
        activeItem: 'test active Item 1',
        list: []
      },
      {
        listName: 'test list name 2',
        activeItem: 'test active Item 2',
        list: []
      }
    ],
    onClick
  }
  const render = (props = {}) =>
    shallowMount(Filter, {
      propsData: {
        ...defaultProps,
        ...props
      }
    })

  describe('render', () => {
    it('snapshot', () => {
      const wrapper = render()
      expect(wrapper.element).toMatchSnapshot()
    })
    it('render filterList item', () => {
      const wrapper = render()
      wrapper.findAll(List).wrappers.forEach((componentWrapper, index) => {
        expect(componentWrapper.props('listName')).toBe(
          defaultProps.filterList[index].listName
        )
        expect(componentWrapper.props('activeItem')).toBe(
          defaultProps.filterList[index].activeItem
        )
        expect(componentWrapper.props('onClick')).toBe(wrapper.props('onClick'))
        expect(componentWrapper.props('list')).toBe(
          defaultProps.filterList[index].list
        )
      })
    })
  })
})
