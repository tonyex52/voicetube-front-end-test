import { shallowMount } from '@vue/test-utils'
import Item from './Item.vue'
import List from './List.vue'

describe('List', () => {
  const onClick = jest.fn()
  const defaultProps = {
    listName: 'test list name',
    activeItem: 'list item value 1',
    onClick,
    list: [
      {
        value: 'list item value 1',
        name: 'list item name 1'
      },
      {
        value: 'list item value 2',
        name: 'list item name 2'
      }
    ]
  }
  const render = (props = {}) =>
    shallowMount(List, {
      propsData: {
        ...defaultProps,
        ...props
      }
    })

  describe('render', () => {
    it('snapshot', () => {
      const wrapper = render()
      expect(wrapper.element).toMatchSnapshot()
    })
    it('render Items from list', () => {
      const wrapper = render()
      wrapper.findAll(Item).wrappers.forEach((componentWrapper, index) => {
        expect(componentWrapper.props('value')).toBe(
          defaultProps.list[index].value
        )
        expect(componentWrapper.props('name')).toBe(
          defaultProps.list[index].name
        )
        expect(componentWrapper.props('isActive')).toBe(
          defaultProps.list[index].value === defaultProps.activeItem
        )
        expect(componentWrapper.props('onClick')).toBe(wrapper.vm.onClickItem)
        expect(componentWrapper.classes('filter-list-item')).toBeTruthy()
      })
    })
  })

  describe('onClickItem', () => {
    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('trigger "onClick"', () => {
      const wrapper = render()
      const item = 'test item'
      wrapper.vm.onClickItem(item)
      expect(onClick).toHaveBeenCalledWith({
        filterType: defaultProps.listName,
        item
      })
    })
  })
})
