import { shallowMount } from '@vue/test-utils'
import Item from './Item.vue'

describe('Item', () => {
  const onClick = jest.fn()
  const defaultProps = {
    isActive: false,
    name: 'test name',
    value: 'test value',
    onClick
  }
  const render = (props = {}) =>
    shallowMount(Item, {
      propsData: {
        ...defaultProps,
        ...props
      }
    })

  describe('render', () => {
    it('snapshot', () => {
      const wrapper = render()
      expect(wrapper.element).toMatchSnapshot()
    })
    it('get class "active" if isActive is true', () => {
      const wrapper = render({ isActive: true })
      expect(wrapper.classes('active')).toBeTruthy()
    })
  })

  describe('onClick', () => {
    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('not trigger "onClick" if isActive is true', () => {
      const wrapper = render({ isActive: true })
      wrapper.trigger('click')
      expect(onClick).toHaveBeenCalledTimes(0)
    })

    it('trigger "onClick" if isActive is false', () => {
      const wrapper = render()
      wrapper.trigger('click')
      expect(onClick).toHaveBeenCalledTimes(1)
    })
  })
})
