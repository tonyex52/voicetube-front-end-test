import { shallowMount } from '@vue/test-utils'
import Label from './Label.vue'

describe('Label', () => {
  const defaultProps = {
    text: 'test text'
  }

  describe('render', () => {
    it('snapshot', () => {
      const wrapper = shallowMount(Label, {
        propsData: defaultProps
      })
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
