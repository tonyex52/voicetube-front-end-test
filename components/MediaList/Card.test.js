import { shallowMount } from '@vue/test-utils'
import '~/plugins/tooltip.js'
import Card, { convertor } from './Card.vue'

describe('Card', () => {
  const defaultProps = {
    thumbnail: 'test thumbnail',
    duration: 123,
    title: 'test title',
    views: 456,
    captions: ['test caption 1', 'test caption 2'],
    level: 789
  }
  const render = (props = {}) =>
    shallowMount(Card, {
      mocks: { $t: (text) => text },
      propsData: {
        ...defaultProps,
        ...props
      }
    })

  describe('render', () => {
    it('snapshot', () => {
      const wrapper = render()
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})

describe('convertor', () => {
  it('get original value if rest is 0', () => {
    const accumulator = {
      rest: 0,
      text: 'test rest 0'
    }
    expect(convertor(accumulator, 60)).toBe(accumulator)
  })

  it('change second to minute if second is 60', () => {
    const accumulator = {
      rest: 60,
      text: ''
    }
    expect(convertor(accumulator, 60)).toEqual({
      rest: 1,
      text: '00'
    })
  })

  it('change second to minute if second is 45', () => {
    const accumulator = {
      rest: 45,
      text: ''
    }
    expect(convertor(accumulator, 60)).toEqual({
      rest: 0,
      text: '45'
    })
  })
})
