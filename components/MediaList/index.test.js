import { shallowMount } from '@vue/test-utils'
import Card from './Card'
import MediaList from './index.vue'

describe('MediaList', () => {
  const defaultProps = {
    mediaList: [
      {
        id: 'test id 1',
        thumbnail: 'test thumbnail 1',
        duration: 123,
        title: 'test title 1',
        views: 456,
        captions: ['test caption 1', 'test caption 2'],
        level: 789
      },
      {
        id: 'test id 2',
        thumbnail: 'test thumbnail 2',
        duration: 987,
        title: 'test title 2',
        views: 654,
        captions: ['test caption 3', 'test caption 4'],
        level: 321
      }
    ]
  }
  const render = (props = {}) =>
    shallowMount(MediaList, {
      mocks: { $t: (text) => text },
      propsData: {
        ...defaultProps,
        ...props
      }
    })

  describe('render', () => {
    it('snapshot', () => {
      const wrapper = render()
      expect(wrapper.element).toMatchSnapshot()
      const emptyWrapper = render({ mediaList: [] })
      expect(emptyWrapper.element).toMatchSnapshot()
    })
    it('render mediaList item', () => {
      const wrapper = render()
      wrapper.findAll(Card).wrappers.forEach((componentWrapper, index) => {
        expect(componentWrapper.props('thumbnail')).toBe(
          defaultProps.mediaList[index].thumbnail
        )
        expect(componentWrapper.props('duration')).toBe(
          defaultProps.mediaList[index].duration
        )
        expect(componentWrapper.props('title')).toBe(
          defaultProps.mediaList[index].title
        )
        expect(componentWrapper.props('views')).toBe(
          defaultProps.mediaList[index].views
        )
        expect(componentWrapper.props('captions')).toBe(
          defaultProps.mediaList[index].captions
        )
        expect(componentWrapper.props('level')).toBe(
          defaultProps.mediaList[index].level
        )
      })
    })
  })
})
