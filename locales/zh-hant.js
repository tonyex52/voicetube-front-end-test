import {
  SORTING_BY_DATE,
  SORTING_BY_VIEW,
  SORTING_BY_FAVORITE_COUNT,
  LENGTH_ALL,
  LENGTH_UNDER_FOUR_MINUTES,
  LENGTH_FROM_FIVE_TO_TEN_MINUTES,
  LENGTH_OVER_TEN_MINUTES
} from '~/constants/filter'

export default {
  filterSort: {
    title: '排序',
    [SORTING_BY_DATE]: '發布時間',
    [SORTING_BY_VIEW]: '觀看次數',
    [SORTING_BY_FAVORITE_COUNT]: '收藏次數'
  },
  filterLength: {
    title: '長度',
    [LENGTH_ALL]: '不限',
    [LENGTH_UNDER_FOUR_MINUTES]: '4 分鐘以下',
    [LENGTH_FROM_FIVE_TO_TEN_MINUTES]: '5 - 10 分鐘',
    [LENGTH_OVER_TEN_MINUTES]: '超過10 分鐘'
  },
  caption: {
    cht: '中文',
    ja: '日文',
    vi: '越南文',
    en: '英文'
  },
  level: {
    '1': '初級',
    '2': '中級',
    '3': '中高級',
    '4': '高級'
  },
  mediaEmpty: '沒有篩選結果'
}
