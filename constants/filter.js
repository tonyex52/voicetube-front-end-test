export const SORTING_BY_DATE = 'sortingByDate'
export const SORTING_BY_VIEW = 'sortingByView'
export const SORTING_BY_FAVORITE_COUNT = 'sortingByFavoriteCount'

export const LENGTH_ALL = 'all'
export const LENGTH_UNDER_FOUR_MINUTES = 'underFourMinutes'
export const LENGTH_FROM_FIVE_TO_TEN_MINUTES = 'fromFiveToTenMinutes'
export const LENGTH_OVER_TEN_MINUTES = 'overTenMinutes'
