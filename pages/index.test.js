import { shallowMount } from '@vue/test-utils'
import PageIndex from './index.vue'
import { SORTING_BY_FAVORITE_COUNT } from '~/constants/filter'
import i18nMessage from '~/locales/zh-hant.js'

jest.mock('axios')

describe('PageIndex', () => {
  const render = () =>
    shallowMount(PageIndex, {
      mocks: { $t: (text) => text }
    })

  describe('render', () => {
    it('snapshot', () => {
      const wrapper = render()
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('onClickFilter', () => {
    it('trigger correct filter', () => {
      const wrapper = render()
      const filterType = i18nMessage.filterSort.title
      const item = SORTING_BY_FAVORITE_COUNT
      const tempFilter = wrapper.vm.filter.slice().map((filterItem) => {
        if (filterItem.listName === filterType) {
          return {
            ...filterItem,
            activeItem: SORTING_BY_FAVORITE_COUNT
          }
        }
        return filterItem
      })
      wrapper.vm.onClickFilter({ filterType, item })
      expect(wrapper.vm.filter).toEqual(tempFilter)
    })

    it('trigger wrong filter', () => {
      const wrapper = render()
      const filterType = 'wrong filter type'
      const item = 'wrong filter item'
      const tempFilter = wrapper.vm.filter.slice().map((filterItem) => {
        if (filterItem.listName === filterType) {
          return {
            ...filterItem,
            activeItem: SORTING_BY_FAVORITE_COUNT
          }
        }
        return filterItem
      })
      wrapper.vm.onClickFilter({ filterType, item })
      expect(wrapper.vm.filter).toEqual(tempFilter)
    })
  })
})
