import { ZH_HANT } from '../constants/i18n'

export const state = () => ({
  locales: [ZH_HANT],
  locale: ZH_HANT
})

export const mutations = {
  SET_LANG(state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale
    }
  }
}
